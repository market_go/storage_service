CREATE TABLE incomes(
    "id" UUID PRIMARY KEY,
    "income_id" VARCHAR UNIQUE NOT NULL,
    "branch" UUID NOT NULL,
    "shipper" UUID NOT NULL,
    "date" DATE,
    "status" VARCHAR DEFAULT 'in_process',
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE income_products(
    "id" UUID PRIMARY KEY,
    "brand" UUID NOT NULL,
    "category" UUID NOT NULL,
    "product_name" UUID NOT NULL,
    "barcode" VARCHAR UNIQUE,
    "quantity" INT,
    "income_price" INT,
    "incomes_id" UUID REFERENCES incomes("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

