--Storage Service
CREATE TABLE income_products(
    "id" UUID PRIMARY KEY,
    "brand" UUID NOT NULL,
    "category" UUID NOT NULL,
    "product_name" UUID NOT NULL,
    "barcode" VARCHAR UNIQUE,
    "quantity" INT,
    "income_price" INT,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE incomes(
    "id" UUID PRIMARY KEY,
    "income_id" VARCHAR UNIQUE NOT NULL,
    "branch" UUID NOT NULL,
    "shipper" UUID NOT NULL,
    "date" DATE,
    "status" VARCHAR DEFAULT 'in_process',
    "income_product" UUID REFERENCES income_products("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE lefts(
    "id" UUID PRIMARY KEY,
    "branch" UUID NOT NULL,
    "brand" UUID NOT NULL,
    "category" UUID NOT NULL,
    "product_name" UUID NOT NULL,
    "barcode" VARCHAR UNIQUE NOT NULL,
    "price_income" INT,
    "quantity" INT,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);