package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/market_go/storage_service/config"
	"gitlab.com/market_go/storage_service/genproto/storage_service"
	"gitlab.com/market_go/storage_service/grpc/client"
	"gitlab.com/market_go/storage_service/grpc/service"
	"gitlab.com/market_go/storage_service/pkg/logger"
	"gitlab.com/market_go/storage_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	storage_service.RegisterIncomeProductServiceServer(grpcServer, service.NewIncomeProductService(cfg, log, strg, srvc))
	storage_service.RegisterIncomeServiceServer(grpcServer, service.NewIncomeService(cfg, log, strg, srvc))
	storage_service.RegisterLeftServiceServer(grpcServer, service.NewLeftService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
