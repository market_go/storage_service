package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/market_go/storage_service/config"
	"gitlab.com/market_go/storage_service/genproto/storage_service"
	"gitlab.com/market_go/storage_service/grpc/client"
	"gitlab.com/market_go/storage_service/pkg/logger"
	"gitlab.com/market_go/storage_service/storage"
)

type LeftService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*storage_service.UnimplementedLeftServiceServer
}

func NewLeftService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *LeftService {
	return &LeftService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *LeftService) Create(ctx context.Context, req *storage_service.LeftCreate) (*storage_service.Left, error) {
	u.log.Info("====== Left Create ======", logger.Any("req", req))

	resp, err := u.strg.Left().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Left: u.strg.Left().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *LeftService) GetById(ctx context.Context, req *storage_service.LeftPrimaryKey) (*storage_service.Left, error) {
	u.log.Info("====== Left Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Left().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Left Get By ID: u.strg.Left().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *LeftService) GetList(ctx context.Context, req *storage_service.LeftGetListRequest) (*storage_service.LeftGetListResponse, error) {
	u.log.Info("====== Left Get List ======", logger.Any("req", req))

	resp, err := u.strg.Left().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Left Get List: u.strg.Left().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *LeftService) Update(ctx context.Context, req *storage_service.LeftUpdate) (*storage_service.Left, error) {
	u.log.Info("====== Left Update ======", logger.Any("req", req))

	resp, err := u.strg.Left().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Left Update: u.strg.Left().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *LeftService) Delete(ctx context.Context, req *storage_service.LeftPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Left Delete ======", logger.Any("req", req))

	err := u.strg.Left().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Left Delete: u.strg.Left().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
