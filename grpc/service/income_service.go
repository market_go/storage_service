package service

import (
	"context"
	"errors"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/market_go/storage_service/config"
	"gitlab.com/market_go/storage_service/genproto/product_service"
	"gitlab.com/market_go/storage_service/genproto/storage_service"
	"gitlab.com/market_go/storage_service/grpc/client"
	"gitlab.com/market_go/storage_service/pkg/logger"
	"gitlab.com/market_go/storage_service/storage"
)

type IncomeService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*storage_service.UnimplementedIncomeServiceServer
}

func NewIncomeService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *IncomeService {
	return &IncomeService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *IncomeService) DoIncomes(ctx context.Context, req *storage_service.IncomePrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== DoIncome ======", logger.Any("req", req))

	// Here Get Income to Update Status
	income, err := u.strg.Income().GetByID(ctx, &storage_service.IncomePrimaryKey{
		Id: req.Id,
	})
	if err != nil {
		u.log.Error("Error While DoIncome: u.strg.Income().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if income.Status == "finished" {
		return nil, errors.New("!Приход уже завершен")
	}
	// Here select from Income_Product and Return ProductId, IncomePrice, Quantity
	resp, err := u.strg.Income().DoIncome(ctx, req)
	if err != nil {
		u.log.Error("Error While DoIncome: u.strg.Income().DoIncome", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	for _, values := range resp.DoIncomes {
		// Here Get Product to Update Product Table Price
		product_get, err := u.services.ProductService().GetById(ctx, &product_service.ProductPrimaryKey{
			Id: values.ProductName,
		})
		if err != nil {
			u.log.Error("Error While DoIncome: u.services.ProductService().GetById", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		// Here Update Product Price
		product_get.Price = values.IncomePrice
		_, err = u.services.ProductService().Update(ctx, &product_service.ProductUpdate{
			Id:       product_get.Id,
			Name:     product_get.Name,
			Photo:    product_get.Photo,
			Category: product_get.Category,
			Brand:    product_get.Brand,
			Barcode:  product_get.Barcode,
			Price:    product_get.Price,
		})
		if err != nil {
			u.log.Error("Error While DoIncome: u.services.ProductService().Update", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		// Here Get Left To Update Left Price And Quantity
		left_get, err := u.strg.Left().GetByID(ctx, &storage_service.LeftPrimaryKey{
			ProductId: values.ProductName,
		})
		if err != nil {
			if err.Error() == "no rows in result set" {
				// Here Get Income to Set Brand in Left Table
				income, err := u.strg.Income().GetByID(ctx, &storage_service.IncomePrimaryKey{
					Id: req.Id,
				})
				if err != nil {
					u.log.Error("Error While DoIncome: u.strg.Income().GetByID", logger.Error(err))
					return nil, status.Error(codes.InvalidArgument, err.Error())
				}
				// Here Create Left if Product Not Exist in the table
				_, err = u.strg.Left().Create(ctx, &storage_service.LeftCreate{
					Branch:      income.Id,
					Brand:       product_get.Brand,
					Category:    product_get.Category,
					ProductName: product_get.Id,
					Barcode:     product_get.Barcode,
					PriceIncome: values.IncomePrice,
					Quantity:    values.Quantity,
				})
				if err != nil {
					u.log.Error("Error While DoIncome: u.strg.Left().Create", logger.Error(err))
					return nil, status.Error(codes.InvalidArgument, err.Error())
				}
				return &emptypb.Empty{}, nil
			}
			u.log.Error("Error While DoIncome: u.services.ProductService().Update", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		// Here Update Left Price and Quantity
		left_get.PriceIncome = values.IncomePrice
		left_get.Quantity += values.Quantity
		_, err = u.strg.Left().Update(ctx, &storage_service.LeftUpdate{
			Id:          left_get.Id,
			Branch:      left_get.Branch,
			Brand:       left_get.Brand,
			Category:    left_get.Category,
			ProductName: left_get.ProductName,
			Barcode:     left_get.Barcode,
			PriceIncome: left_get.PriceIncome,
			Quantity:    left_get.Quantity,
		})
		if err != nil {
			u.log.Error("Error While DoIncome: u.strg.Left().Update", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	}

	// Here Update Income Status to Finished
	_, err = u.strg.Income().Update(ctx, &storage_service.IncomeUpdate{
		Id:      income.Id,
		Branch:  income.Branch,
		Shipper: income.Shipper,
		Date:    income.Date,
		Status:  "finished",
	})
	if err != nil {
		u.log.Error("Error While DoIncome: u.strg.Income().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return &emptypb.Empty{}, nil
}

func (u *IncomeService) Create(ctx context.Context, req *storage_service.IncomeCreate) (*storage_service.Income, error) {
	u.log.Info("====== Income Create ======", logger.Any("req", req))

	resp, err := u.strg.Income().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Income: u.strg.Income().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *IncomeService) GetById(ctx context.Context, req *storage_service.IncomePrimaryKey) (*storage_service.Income, error) {
	u.log.Info("====== Income Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Income().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Income Get By ID: u.strg.Income().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *IncomeService) GetList(ctx context.Context, req *storage_service.IncomeGetListRequest) (*storage_service.IncomeGetListResponse, error) {
	u.log.Info("====== Income Get List ======", logger.Any("req", req))

	resp, err := u.strg.Income().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Income Get List: u.strg.Income().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *IncomeService) Update(ctx context.Context, req *storage_service.IncomeUpdate) (*storage_service.Income, error) {
	u.log.Info("====== Income Update ======", logger.Any("req", req))

	resp, err := u.strg.Income().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Income Update: u.strg.Income().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *IncomeService) Delete(ctx context.Context, req *storage_service.IncomePrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Income Delete ======", logger.Any("req", req))

	err := u.strg.Income().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Income Delete: u.strg.Income().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
