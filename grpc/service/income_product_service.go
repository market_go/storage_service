package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/market_go/storage_service/config"
	"gitlab.com/market_go/storage_service/genproto/storage_service"
	"gitlab.com/market_go/storage_service/grpc/client"
	"gitlab.com/market_go/storage_service/pkg/logger"
	"gitlab.com/market_go/storage_service/storage"
)

type IncomeProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*storage_service.UnimplementedIncomeProductServiceServer
}

func NewIncomeProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *IncomeProductService {
	return &IncomeProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *IncomeProductService) Create(ctx context.Context, req *storage_service.IncomeProductCreate) (*storage_service.IncomeProduct, error) {
	u.log.Info("====== IncomeProduct Create ======", logger.Any("req", req))

	resp, err := u.strg.IncomeProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create IncomeProduct: u.strg.IncomeProduct().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *IncomeProductService) GetById(ctx context.Context, req *storage_service.IncomeProductPrimaryKey) (*storage_service.IncomeProduct, error) {
	u.log.Info("====== IncomeProduct Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.IncomeProduct().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While IncomeProduct Get By ID: u.strg.IncomeProduct().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *IncomeProductService) GetList(ctx context.Context, req *storage_service.IncomeProductGetListRequest) (*storage_service.IncomeProductGetListResponse, error) {
	u.log.Info("====== IncomeProduct Get List ======", logger.Any("req", req))

	resp, err := u.strg.IncomeProduct().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While IncomeProduct Get List: u.strg.IncomeProduct().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *IncomeProductService) Update(ctx context.Context, req *storage_service.IncomeProductUpdate) (*storage_service.IncomeProduct, error) {
	u.log.Info("====== IncomeProduct Update ======", logger.Any("req", req))

	resp, err := u.strg.IncomeProduct().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While IncomeProduct Update: u.strg.IncomeProduct().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *IncomeProductService) Delete(ctx context.Context, req *storage_service.IncomeProductPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== IncomeProduct Delete ======", logger.Any("req", req))

	err := u.strg.IncomeProduct().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While IncomeProduct Delete: u.strg.IncomeProduct().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
