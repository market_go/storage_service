package client

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/market_go/storage_service/config"
	"gitlab.com/market_go/storage_service/genproto/organization_service"
	"gitlab.com/market_go/storage_service/genproto/product_service"
)

type ServiceManagerI interface {
	// Organization Services
	BranchService() organization_service.BranchServiceClient
	MagazineService() organization_service.MagazineServiceClient
	StaffService() organization_service.StaffServiceClient
	ShipperService() organization_service.ShipperServiceClient

	// Product Services
	BrandService() product_service.BrandServiceClient
	CategoryService() product_service.CategoryServiceClient
	ProductService() product_service.ProductServiceClient
}

type grpcClients struct {
	// Organization Services
	branchService   organization_service.BranchServiceClient
	magazineService organization_service.MagazineServiceClient
	staffService    organization_service.StaffServiceClient
	shipperService  organization_service.ShipperServiceClient

	// Product Services
	brandService    product_service.BrandServiceClient
	categoryService product_service.CategoryServiceClient
	productService  product_service.ProductServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Organization Microservice connection
	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationServiceHost+cfg.OrganizationGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Product Microservice connection
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Organization Service
		branchService:   organization_service.NewBranchServiceClient(connOrganizationService),
		magazineService: organization_service.NewMagazineServiceClient(connOrganizationService),
		staffService:    organization_service.NewStaffServiceClient(connOrganizationService),
		shipperService:  organization_service.NewShipperServiceClient(connOrganizationService),
		// Product Service
		brandService:    product_service.NewBrandServiceClient(connProductService),
		categoryService: product_service.NewCategoryServiceClient(connProductService),
		productService:  product_service.NewProductServiceClient(connProductService),
	}, nil
}

// Organization Services

func (g *grpcClients) BranchService() organization_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) MagazineService() organization_service.MagazineServiceClient {
	return g.magazineService
}

func (g *grpcClients) StaffService() organization_service.StaffServiceClient {
	return g.staffService
}

func (g *grpcClients) ShipperService() organization_service.ShipperServiceClient {
	return g.shipperService
}

// Product Services

func (g *grpcClients) BrandService() product_service.BrandServiceClient {
	return g.brandService
}

func (g *grpcClients) CategoryService() product_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}
