package storage

import (
	"context"

	"gitlab.com/market_go/storage_service/genproto/storage_service"
)

type StorageI interface {
	CloseDB()
	IncomeProduct() IncomeProductRepoI
	Income() IncomeRepoI
	Left() LeftRepoI
}

type IncomeProductRepoI interface {
	Create(context.Context, *storage_service.IncomeProductCreate) (*storage_service.IncomeProduct, error)
	GetByID(context.Context, *storage_service.IncomeProductPrimaryKey) (*storage_service.IncomeProduct, error)
	GetList(context.Context, *storage_service.IncomeProductGetListRequest) (*storage_service.IncomeProductGetListResponse, error)
	Update(context.Context, *storage_service.IncomeProductUpdate) (*storage_service.IncomeProduct, error)
	Delete(context.Context, *storage_service.IncomeProductPrimaryKey) error
}

type IncomeRepoI interface {
	DoIncome(ctx context.Context, req *storage_service.IncomePrimaryKey) (*storage_service.DoIncome, error)

	Create(context.Context, *storage_service.IncomeCreate) (*storage_service.Income, error)
	GetByID(context.Context, *storage_service.IncomePrimaryKey) (*storage_service.Income, error)
	GetList(context.Context, *storage_service.IncomeGetListRequest) (*storage_service.IncomeGetListResponse, error)
	Update(context.Context, *storage_service.IncomeUpdate) (*storage_service.Income, error)
	Delete(context.Context, *storage_service.IncomePrimaryKey) error
}

type LeftRepoI interface {
	Create(context.Context, *storage_service.LeftCreate) (*storage_service.Left, error)
	GetByID(context.Context, *storage_service.LeftPrimaryKey) (*storage_service.Left, error)
	GetList(context.Context, *storage_service.LeftGetListRequest) (*storage_service.LeftGetListResponse, error)
	Update(context.Context, *storage_service.LeftUpdate) (*storage_service.Left, error)
	Delete(context.Context, *storage_service.LeftPrimaryKey) error
}
