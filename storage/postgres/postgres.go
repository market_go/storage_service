package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/storage_service/config"
	"gitlab.com/market_go/storage_service/storage"
)

type Store struct {
	db             *pgxpool.Pool
	income_product *IncomeProductRepo
	income         *IncomeRepo
	left           *LeftRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil

}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) IncomeProduct() storage.IncomeProductRepoI {
	if s.income_product == nil {
		s.income_product = NewIncomeProductRepo(s.db)
	}

	return s.income_product
}

func (s *Store) Income() storage.IncomeRepoI {
	if s.income == nil {
		s.income = NewIncomeRepo(s.db)
	}

	return s.income
}

func (s *Store) Left() storage.LeftRepoI {
	if s.left == nil {
		s.left = NewLeftRepo(s.db)
	}

	return s.left
}
