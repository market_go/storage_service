package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/storage_service/genproto/storage_service"
	"gitlab.com/market_go/storage_service/pkg/helper"
)

type IncomeRepo struct {
	db *pgxpool.Pool
}

func NewIncomeRepo(db *pgxpool.Pool) *IncomeRepo {
	return &IncomeRepo{
		db: db,
	}
}

func (r *IncomeRepo) DoIncome(ctx context.Context, req *storage_service.IncomePrimaryKey) (*storage_service.DoIncome, error) {
	var (
		query string
		resp  = &storage_service.DoIncome{}
		where = ""
	)
	query = `
		SELECT
			product_name,
			quantity,
			income_price
		FROM income_products
	`
	where = fmt.Sprintf(" WHERE incomes_id = '%s'", req.Id)

	query += where
	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			ProductName sql.NullString
			Quantity    sql.NullInt64
			IncomePrice sql.NullInt64
		)

		err := rows.Scan(
			&ProductName,
			&Quantity,
			&IncomePrice,
		)

		if err != nil {
			return nil, err
		}

		resp.DoIncomes = append(resp.DoIncomes, &storage_service.DoIncomeInfos{
			ProductName: ProductName.String,
			Quantity:    Quantity.Int64,
			IncomePrice: IncomePrice.Int64,
		})
	}
	rows.Close()
	fmt.Println(resp)
	return resp, nil
}

func (r *IncomeRepo) Create(ctx context.Context, req *storage_service.IncomeCreate) (*storage_service.Income, error) {

	count, err := r.GetList(ctx, &storage_service.IncomeGetListRequest{})
	if err != nil {
		return nil, err
	}

	var (
		id        = uuid.New().String()
		income_id = helper.GenerateString("П", int(count.Count)+1)
		query     string
	)
	query = `
		INSERT INTO incomes(id, income_id, branch, shipper, date, updated_at)
		VALUES ($1, $2, $3, $4, $5, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		income_id,
		helper.NewNullString(req.Branch),
		helper.NewNullString(req.Shipper),
		req.Date,
	)

	if err != nil {
		return nil, err
	}

	return &storage_service.Income{
		Id:       id,
		IncomeId: income_id,
		Branch:   req.Branch,
		Shipper:  req.Shipper,
		Date:     req.Date,
	}, nil
}

func (r *IncomeRepo) GetByID(ctx context.Context, req *storage_service.IncomePrimaryKey) (*storage_service.Income, error) {

	var (
		query string

		Id        sql.NullString
		IncomeId  sql.NullString
		Branch    sql.NullString
		Shipper   sql.NullString
		Date      sql.NullString
		Status    sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	query = `
		SELECT
			id,
			income_id,
			branch,
			shipper,
			date,
			status,
			created_at,
			updated_at
		FROM incomes
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&IncomeId,
		&Branch,
		&Shipper,
		&Date,
		&Status,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &storage_service.Income{
		Id:        Id.String,
		IncomeId:  IncomeId.String,
		Branch:    Branch.String,
		Shipper:   Shipper.String,
		Date:      Date.String,
		Status:    Status.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *IncomeRepo) GetList(ctx context.Context, req *storage_service.IncomeGetListRequest) (*storage_service.IncomeGetListResponse, error) {

	var (
		resp   = &storage_service.IncomeGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			income_id,
			branch,
			shipper,
			date,
			status,
			created_at,
			updated_at	
		FROM incomes
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBranch != "" {
		where += ` AND branch ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	if req.SearchByIncomeId != "" {
		where += ` AND income_id ILIKE '%' || '` + req.SearchByIncomeId + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			IncomeId  sql.NullString
			Branch    sql.NullString
			Shipper   sql.NullString
			Date      sql.NullString
			Status    sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&IncomeId,
			&Branch,
			&Shipper,
			&Date,
			&Status,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Incomes = append(resp.Incomes, &storage_service.Income{
			Id:        Id.String,
			IncomeId:  IncomeId.String,
			Branch:    Branch.String,
			Shipper:   Shipper.String,
			Date:      Date.String,
			Status:    Status.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *IncomeRepo) Update(ctx context.Context, req *storage_service.IncomeUpdate) (*storage_service.Income, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			incomes
		SET
			branch = :branch,
			shipper = :shipper,
			date = :date,
			status = :status,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":      req.Id,
		"branch":  helper.NewNullString(req.Branch),
		"shipper": helper.NewNullString(req.Shipper),
		"date":    req.Date,
		"status":  req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &storage_service.Income{
		Id:      req.Id,
		Branch:  req.Branch,
		Shipper: req.Shipper,
		Date:    req.Date,
		Status:  req.Status,
	}, nil
}

func (r *IncomeRepo) Delete(ctx context.Context, req *storage_service.IncomePrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM incomes WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
