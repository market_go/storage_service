package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/storage_service/genproto/storage_service"
	"gitlab.com/market_go/storage_service/pkg/helper"
)

type LeftRepo struct {
	db *pgxpool.Pool
}

func NewLeftRepo(db *pgxpool.Pool) *LeftRepo {
	return &LeftRepo{
		db: db,
	}
}

func (r *LeftRepo) Create(ctx context.Context, req *storage_service.LeftCreate) (*storage_service.Left, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO lefts(id, branch, brand, category, product_name, barcode, price_income, quantity, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.Branch),
		helper.NewNullString(req.Brand),
		helper.NewNullString(req.Category),
		helper.NewNullString(req.ProductName),
		req.Barcode,
		req.PriceIncome,
		req.Quantity,
	)

	if err != nil {
		return nil, err
	}

	return &storage_service.Left{
		Id:          id,
		Branch:      req.Branch,
		Brand:       req.Brand,
		Category:    req.Category,
		ProductName: req.ProductName,
		Barcode:     req.Barcode,
		PriceIncome: req.PriceIncome,
		Quantity:    req.Quantity,
	}, nil
}

func (r *LeftRepo) GetByID(ctx context.Context, req *storage_service.LeftPrimaryKey) (*storage_service.Left, error) {

	var (
		query    string
		where    string
		argument string

		Id          sql.NullString
		Branch      sql.NullString
		Brand       sql.NullString
		Category    sql.NullString
		ProductName sql.NullString
		Barcode     sql.NullString
		PriceIncome sql.NullInt64
		Quantity    sql.NullInt64
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	if len(req.ProductId) > 0 {
		where = " WHERE product_name = $1"
		argument = req.ProductId
	} else if len(req.Id) > 0 {
		where = " WHERE id = $1"
		argument = req.Id
	}

	query = `
		SELECT
			id,
			branch,
			brand,
			category,
			product_name,
			barcode,
			price_income,
			quantity,
			created_at,
			updated_at
		FROM lefts
	`
	query += where

	err := r.db.QueryRow(ctx, query, argument).Scan(
		&Id,
		&Branch,
		&Brand,
		&Category,
		&ProductName,
		&Barcode,
		&PriceIncome,
		&Quantity,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &storage_service.Left{
		Id:          Id.String,
		Branch:      Branch.String,
		Brand:       Brand.String,
		Category:    Category.String,
		ProductName: ProductName.String,
		Barcode:     Barcode.String,
		PriceIncome: PriceIncome.Int64,
		Quantity:    Quantity.Int64,
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}, nil
}

func (r *LeftRepo) GetList(ctx context.Context, req *storage_service.LeftGetListRequest) (*storage_service.LeftGetListResponse, error) {

	var (
		resp   = &storage_service.LeftGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch,
			brand,
			category,
			product_name,
			barcode,
			price_income,
			quantity,
			created_at,
			updated_at	
		FROM lefts
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBranch != "" {
		where += ` AND branch ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	if req.SearchByBrand != "" {
		where += ` AND brand ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	if req.SerachByBarcode != "" {
		where += ` AND barcode ILIKE '%' || '` + req.SerachByBarcode + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			Branch      sql.NullString
			Brand       sql.NullString
			Category    sql.NullString
			ProductName sql.NullString
			Barcode     sql.NullString
			PriceIncome sql.NullInt64
			Quantity    sql.NullInt64
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&Branch,
			&Brand,
			&Category,
			&ProductName,
			&Barcode,
			&PriceIncome,
			&Quantity,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Lefts = append(resp.Lefts, &storage_service.Left{
			Id:          Id.String,
			Branch:      Branch.String,
			Brand:       Brand.String,
			Category:    Category.String,
			ProductName: ProductName.String,
			Barcode:     Barcode.String,
			PriceIncome: PriceIncome.Int64,
			Quantity:    Quantity.Int64,
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *LeftRepo) Update(ctx context.Context, req *storage_service.LeftUpdate) (*storage_service.Left, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			lefts
		SET
			branch = :branch,
			brand = :brand,
			category = :category,
			product_name = :product_name,
			barcode = :barcode,
			price_income = :price_income,
			quantity = :quantity,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"branch":       req.Branch,
		"brand":        req.Brand,
		"category":     req.Category,
		"product_name": req.ProductName,
		"barcode":      req.Barcode,
		"price_income": req.PriceIncome,
		"quantity":     req.Quantity,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &storage_service.Left{
		Id:          req.Id,
		Branch:      req.Branch,
		Brand:       req.Brand,
		Category:    req.Category,
		ProductName: req.ProductName,
		Barcode:     req.Barcode,
		PriceIncome: req.PriceIncome,
		Quantity:    req.Quantity,
	}, nil
}

func (r *LeftRepo) Delete(ctx context.Context, req *storage_service.LeftPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM lefts WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
