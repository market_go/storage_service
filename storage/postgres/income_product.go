package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/storage_service/genproto/storage_service"
	"gitlab.com/market_go/storage_service/pkg/helper"
)

type IncomeProductRepo struct {
	db *pgxpool.Pool
}

func NewIncomeProductRepo(db *pgxpool.Pool) *IncomeProductRepo {
	return &IncomeProductRepo{
		db: db,
	}
}

func (r *IncomeProductRepo) Create(ctx context.Context, req *storage_service.IncomeProductCreate) (*storage_service.IncomeProduct, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO income_products(id, brand, category, product_name, barcode, quantity, income_price, incomes_id, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.Brand),
		helper.NewNullString(req.Category),
		helper.NewNullString(req.ProductName),
		req.Barcode,
		req.Quantity,
		req.IncomePrice,
		req.IncomeId,
	)

	if err != nil {
		return nil, err
	}

	return &storage_service.IncomeProduct{
		Id:          id,
		Brand:       req.Brand,
		Category:    req.Category,
		ProductName: req.ProductName,
		Barcode:     req.Barcode,
		Quantity:    req.Quantity,
		IncomePrice: req.IncomePrice,
		IncomeId:    req.IncomeId,
	}, nil
}

func (r *IncomeProductRepo) GetByID(ctx context.Context, req *storage_service.IncomeProductPrimaryKey) (*storage_service.IncomeProduct, error) {

	var (
		query string

		Id          sql.NullString
		Brand       sql.NullString
		Category    sql.NullString
		ProductName sql.NullString
		Barcode     sql.NullString
		Quantity    sql.NullInt64
		IncomePrice sql.NullInt64
		IncomeId    sql.NullString
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	query = `
		SELECT
			id,
			brand,
			category,
			product_name,
			barcode,
			quantity,
			income_price,
			incomes_id,
			created_at,
			updated_at
		FROM income_products
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&Brand,
		&Category,
		&ProductName,
		&Barcode,
		&Quantity,
		&IncomePrice,
		&IncomeId,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &storage_service.IncomeProduct{
		Id:          Id.String,
		Brand:       Brand.String,
		Category:    Category.String,
		ProductName: ProductName.String,
		Barcode:     Barcode.String,
		Quantity:    Quantity.Int64,
		IncomePrice: IncomePrice.Int64,
		IncomeId:    IncomeId.String,
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}, nil
}

func (r *IncomeProductRepo) GetList(ctx context.Context, req *storage_service.IncomeProductGetListRequest) (*storage_service.IncomeProductGetListResponse, error) {

	var (
		resp   = &storage_service.IncomeProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			brand,
			category,
			product_name,
			barcode,
			quantity,
			income_price,
			incomes_id,
			created_at,
			updated_at	
		FROM income_products
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBrand != "" {
		where += ` AND brand ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	if req.SerachByBarcode != "" {
		where += ` AND barcode ILIKE '%' || '` + req.SerachByBarcode + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			Brand       sql.NullString
			Category    sql.NullString
			ProductName sql.NullString
			Barcode     sql.NullString
			Quantity    sql.NullInt64
			IncomePrice sql.NullInt64
			IncomeId    sql.NullString
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&Brand,
			&Category,
			&ProductName,
			&Barcode,
			&Quantity,
			&IncomePrice,
			&IncomeId,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.IncomeProducts = append(resp.IncomeProducts, &storage_service.IncomeProduct{
			Id:          Id.String,
			Brand:       Brand.String,
			Category:    Category.String,
			ProductName: ProductName.String,
			Barcode:     Barcode.String,
			Quantity:    Quantity.Int64,
			IncomePrice: IncomePrice.Int64,
			IncomeId:    IncomeId.String,
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *IncomeProductRepo) Update(ctx context.Context, req *storage_service.IncomeProductUpdate) (*storage_service.IncomeProduct, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			income_products
		SET
			brand = :brand,
			category = :category,
			product_name = :product_name,
			barcode = :barcode,
			quantity = :quantity,
			income_price = :income_price,
			incomes_id = :incomes_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"brand":        helper.NewNullString(req.Brand),
		"category":     helper.NewNullString(req.Category),
		"product_name": helper.NewNullString(req.ProductName),
		"barcode":      req.Barcode,
		"quantity":     req.Quantity,
		"income_price": req.IncomePrice,
		"incomes_id":   req.IncomeId,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &storage_service.IncomeProduct{
		Id:          req.Id,
		Brand:       req.Brand,
		Category:    req.Category,
		ProductName: req.ProductName,
		Barcode:     req.Barcode,
		Quantity:    req.Quantity,
		IncomePrice: req.IncomePrice,
		IncomeId:    req.IncomeId,
	}, nil
}

func (r *IncomeProductRepo) Delete(ctx context.Context, req *storage_service.IncomeProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM income_products WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
